## Silver Bars Exercise

Maven project to implement "Live Order Board" exercise.

Class "Order" that contains the fields mentioned in the exercise.

Class "OrderBoard" implements the functionality to group and sort the orders by price. Since Buy orders and Sell orders must be displayed separately, they are stored and manipulated separately.

Class "OrderSample" contains one example in its main method where most of the functionality is used.

Test class "OrderBoardTest" runs different scenarios to test the public methods.

## Author
Pablo Valtuille