public class Order {

    private String type;
    private String userId;
    private double quantity;
    private double price;
    private String id;

    //Using Strings instead of enum for order type since it is simpler and the types are only defined once
    public static final String BUY = "BUY";
    public static final String SELL = "SELL";

    public static final String CURRENCY = "£";
    public static final String QUANTITY_UNIT = "kg";

    private Order(String type, String userId, double quantity, double price) {
        this.type = type;
        this.userId = userId;
        this.quantity = quantity;
        this.price = price;
        this.id = java.util.UUID.randomUUID().toString();
    }

    public static Order createBuyOrder(String userId, double quantity, double price) {
        return new Order(BUY, userId, quantity, price);
    }

    public static Order createSellOrder(String userId, double quantity, double price) {
        return new Order(SELL, userId, quantity, price);
    }

    public String getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (Double.compare(order.quantity, quantity) != 0) return false;
        if (Double.compare(order.price, price) != 0) return false;
        if (type != null ? !type.equals(order.type) : order.type != null) return false;
        if (userId != null ? !userId.equals(order.userId) : order.userId != null) return false;
        return id != null ? id.equals(order.id) : order.id == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = type != null ? type.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        temp = Double.doubleToLongBits(quantity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "type='" + type + '\'' +
                ", userId='" + userId + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", id='" + id + '\'' +
                '}';
    }

}
