import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderBoard {
    private static final String boardMessageIntro = "Live Order Board for: ";

    List<Order> buyOrders = new ArrayList<>();
    List<Order> sellOrders = new ArrayList<>();

    public List<Order> getBuyOrders() {
        return buyOrders;
    }

    public List<Order> getSellOrders() {
        return sellOrders;
    }

    public void addBuyOrder(String userId, double quantity, double price) {
        buyOrders.add(Order.createBuyOrder(userId, quantity, price));
    }

    public void addSellOrder(String userId, double quantity, double price) {
        sellOrders.add(Order.createBuyOrder(userId, quantity, price));
    }

    public boolean cancelOrderIfExists(String orderId) {
        Optional<Order> optOrder = getOrderById(orderId, buyOrders);
        if (optOrder.isPresent()) {
            cancelOrder(optOrder.get(), buyOrders, Order.BUY);
            return true;
        }
        optOrder = getOrderById(orderId, sellOrders);
        if (optOrder.isPresent()) {
            cancelOrder(optOrder.get(), sellOrders, Order.SELL);
            return true;
        }
        System.out.println("Cannot cancel order with id " + orderId + ". Not found");
        return false;
    }

    private Optional<Order> getOrderById(String orderId, List<Order> orderList) {
        return orderList.stream().filter(o -> o.getId().equals(orderId)).findFirst();
    }

    private void cancelOrder(Order order, List<Order> orders, String orderType) {
        System.out.println("Cancelling " + orderType + " order " + order.getId());
        orders.remove(order);
    }

    public void displayOrders() {
        System.out.println(boardMessageIntro + Order.BUY);
        getBuyLiveOrders().forEach(o -> System.out.println(o));
        System.out.println(boardMessageIntro + Order.SELL);
        getSellLiveOrders().forEach(o -> System.out.println(o));
    }

    public List<String> getBuyLiveOrders() {
        Map<Double, List<Order>> priceToOrders = new TreeMap<>((a, b) -> b.compareTo(a)); //reverse price order for Buys
        return getLiveOrders(priceToOrders, buyOrders).collect(Collectors.toList());
    }

    public List<String> getSellLiveOrders() {
        SortedMap<Double, List<Order>> priceToOrders = new TreeMap<>();
        return getLiveOrders(priceToOrders, sellOrders).collect(Collectors.toList());
    }

    private Stream<String> getLiveOrders(Map<Double, List<Order>> priceToOrders, List<Order> orders) {
        priceToOrders.putAll(orders.stream().collect(Collectors.groupingBy(Order::getPrice)));
        return getSortedOrders(priceToOrders);
    }

    public Stream<String> getSortedOrders(Map<Double, List<Order>> priceToOrders) {
        return priceToOrders.entrySet().stream().map(e -> getDisplayEntry(e.getKey(), e.getValue()));
    }

    private String getDisplayEntry(double price, List<Order> orders) {
        return sumQuantity(orders) + " " + Order.QUANTITY_UNIT + " for " +
                Order.CURRENCY + price + " // " + displayOrdersSamePrice(orders);
    }

    private String displayOrdersSamePrice(List<Order> orders) {
        return orders.stream().map(Order::getId).map(id -> "order " + id).collect(Collectors.joining(" + "));
    }

    private static double sumQuantity(List<Order> orders) {
        return orders.stream().mapToDouble(Order::getQuantity).sum();
    }

}
