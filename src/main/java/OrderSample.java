public class OrderSample {

    public static void main(String[] args) {
        OrderBoard orderBoard = new OrderBoard();
        String userId = "123";
        int quantity = 14;
        orderBoard.addBuyOrder(userId, quantity, 44);
        orderBoard.addBuyOrder(userId, quantity, 3);
        orderBoard.addBuyOrder(userId, quantity, 5);
        orderBoard.addBuyOrder("555", quantity, 3);
        orderBoard.addSellOrder(userId, quantity, 44);
        orderBoard.addSellOrder(userId, quantity, 3);
        orderBoard.addSellOrder(userId, quantity, 5);
        orderBoard.addSellOrder("555", quantity, 3);
        orderBoard.displayOrders();
        orderBoard.cancelOrderIfExists("77");
        orderBoard.cancelOrderIfExists(orderBoard.buyOrders.get(1).getId());
        orderBoard.displayOrders();
    }
}
