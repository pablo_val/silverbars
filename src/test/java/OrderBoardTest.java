import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class OrderBoardTest {
    static OrderBoard orderBoard;

    @Before
    public void setup() {
        orderBoard = new OrderBoard();
    }

    private static void addBuyOrder(int price) {
        orderBoard.addBuyOrder("user", 10, price);
    }

    private static void addSellOrder(int price) {
        orderBoard.addSellOrder("user", 10, price);
    }

    @Test
    public void testOneBuyOrderIsDisplayed() {
        addBuyOrder(2);
        assertThat(orderBoard.getBuyOrders().size(), is(1));
        assertThat(orderBoard.getBuyLiveOrders().size(), is(1));
    }

    @Test
    public void testOneSellOrderIsDisplayed() {
        addSellOrder(2);
        assertThat(orderBoard.getSellOrders().size(), is(1));
        assertThat(orderBoard.getSellLiveOrders().size(), is(1));
    }

    @Test
    public void testTwoSellOrdersDifferentPriceAreNotMerged() {
        addSellOrder(2);
        addSellOrder(3);
        assertThat(orderBoard.getSellOrders().size(), is(2));
        assertThat(orderBoard.getSellLiveOrders().size(), is(2));
    }

    @Test
    public void testTwoSellOrdersSamePriceAreMerged() {
        addSellOrder(2);
        addSellOrder(2);
        assertThat(orderBoard.getSellOrders().size(), is(2));
        assertThat(orderBoard.getSellLiveOrders().size(), is(1));
    }

    @Test
    public void testTwoBuyOrdersSamePriceAreMerged() {
        addBuyOrder(2);
        addBuyOrder(2);
        assertThat(orderBoard.getBuyOrders().size(), is(2));
        assertThat(orderBoard.getBuyLiveOrders().size(), is(1));
    }

    @Test
    public void testCancelNonExistingOrder() {
        addBuyOrder(2);
        String nonExistOrderId = "123";
        assertThat(orderBoard.getBuyOrders().size(), is(1));
        assertThat(orderBoard.cancelOrderIfExists(nonExistOrderId), is(false));
        assertThat(orderBoard.getBuyOrders().size(), is(1));
    }

    @Test
    public void testCancelExistingOrderIsRemoved() {
        addBuyOrder(2);
        String existingOrderId = orderBoard.buyOrders.get(0).getId();
        assertThat(orderBoard.getBuyOrders().size(), is(1));
        assertThat(orderBoard.cancelOrderIfExists(existingOrderId), is(true));
        assertThat(orderBoard.getBuyOrders().size(), is(0));
    }

}